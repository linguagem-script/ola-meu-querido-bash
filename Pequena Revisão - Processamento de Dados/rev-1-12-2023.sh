#!/bin/bash

# Filtrar as mil primeiras e últimas linhas e a existencia do access.log 
if [ -e "access.log" ]; then
	head -n 1000 access.log > linhas.log
	tail -n 1000 access.log >> linhas.log
else
	# Baixando o arquivo caso o access.log não exista
	wget https://raw.githubusercontent.com/linuxacademy/content-elastic-log-samples/master/access.log
	head -n 1000 access.log > linhas.log
        tail -n 1000 access.log >> linhas.log
fi

# Removendo o arquivo
rm access.log

# Filtrar o grupo de IPs 
ips=$(grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" linhas.log)

echo $ips | tr ' ' '\n' | sort -u >> teste

# Realizar apenas um único ping
for ip in $(cat teste); do
	ping -c 1 $ip
done
