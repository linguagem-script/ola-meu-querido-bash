#!/bin/bash

arquivo_zip=""

while true; do
    clear
    echo "Selecione uma opção:"
    echo "1 - Escolher um arquivo zip existente"
    echo "2 - Criar um novo arquivo zip"
    echo "3 - Sair"

    read -p "Opção: " opcao

    if [[ $opcao == "1" ]]; then
        read -p "Digite o nome do arquivo zip existente: " arquivo_zip
        if [[ -f $arquivo_zip ]]; then
            break
        else
            echo "Arquivo não encontrado."
        fi
    elif [[ $opcao == "2" ]]; then
        read -p "Digite o nome do novo arquivo zip: " novo_arquivo_zip
        zip $novo_arquivo_zip *
        if [[ -f $novo_arquivo_zip ]]; then
            arquivo_zip=$novo_arquivo_zip
            break
        else
            echo "Falha ao criar o arquivo zip."
        fi
    elif [[ $opcao == "3" ]]; then
        exit
    else
        echo "Opção inválida. Por favor, escolha uma opção válida."
    fi
done

while true; do
    clear
    echo -e "\e[1;33mArquivo ZIP Selecionado: $arquivo_zip\e[0m"
    echo "1 - Listar o conteúdo do arquivo zip"
    echo "2 - Pré-visualizar um arquivo do zip"
    echo "3 - Adicionar arquivos ao zip"
    echo "4 - Remover arquivos do zip"
    echo "5 - Extrair todo o conteúdo do zip"
    echo "6 - Extrair arquivos específicos do zip"
    echo "7 - Voltar ao menu principal"

    read -p "Opção: " operacao

    if [[ $operacao == "1" ]]; then
        if [[ -f $arquivo_zip ]]; then
            lista_zipinfo=$(zipinfo $arquivo_zip)
            echo "$lista_zipinfo"
        else
            echo "Arquivo zip não encontrado."
        fi
    elif [[ $operacao == "2" ]]; then
        read -p "Digite o nome do arquivo a ser pré-visualizado: " arquivo
        if [[ -f $arquivo_zip ]]; then
            unzip -p $arquivo_zip $arquivo | less
            echo "Pressione 'q' para sair do visualizador."
            read -n 1 -s -r -p "Pressione qualquer tecla para continuar..."
        else
            echo "Arquivo zip não encontrado."
        fi
    elif [[ $operacao == "3" ]]; then
        read -p "Digite o caminho do arquivo a ser adicionado: " arquivo_adicionar
        if [[ -f $arquivo_zip && -f $arquivo_adicionar ]]; then
            zip -u $arquivo_zip $arquivo_adicionar
        else
            echo "Arquivo zip ou arquivo a ser adicionado não encontrado."
        fi
    elif [[ $operacao == "4" ]]; then
        read -p "Digite o nome do arquivo a ser removido: " arquivo_remover
        if [[ -f $arquivo_zip ]]; then
            zip -d $arquivo_zip $arquivo_remover
        else
            echo "Arquivo zip não encontrado."
        fi
    elif [[ $operacao == "5" ]]; then
        if [[ -f $arquivo_zip ]]; then
            unzip $arquivo_zip
        else
            echo "Arquivo zip não encontrado."
        fi
    elif [[ $operacao == "6" ]]; then
        read -p "Digite o nome do arquivo a ser extraído: " arquivo_extrair
        if [[ -f $arquivo_zip ]]; then
            unzip $arquivo_zip $arquivo_extrair
        else
            echo "Arquivo zip não encontrado."
        fi
    elif [[ $operacao == "7" ]]; then
        break
    else
        echo "Opção inválida. Por favor, escolha uma opção válida."
    fi
done

