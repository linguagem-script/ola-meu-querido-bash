#!/bin/bash

source func.sh

if [ $# -ne 2 ]; then
    echo "Uso: $0 <numero> <arquivo>"
    exit 1
fi

numero=$1
arquivo=$2

if [ ! -f "$arquivo" ]; then
    echo "O arquivo '$arquivo' não existe."
    exit 1
fi

echo "Escolha entre linhas ou colunas (l/c):"
read escolha

if [ "$escolha" = "l" ]; then
    linha $numero $arquivo
elif [ "$escolha" = "c" ]; then
    coluna $numero $arquivo
else
    echo "Escolha inválida. Digite 'l' para linhas ou 'c' para colunas."
    exit 1
fi

