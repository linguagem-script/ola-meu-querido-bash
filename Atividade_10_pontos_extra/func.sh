#!/bin/bash

linha() {
    if [ $# -ne 2 ]; then
        echo "Uso: linha <numero> <arquivo>"
        return 1
    fi

    local numero=$1
    local arquivo=$2

    if [ ! -f "$arquivo" ]; then
        echo "O arquivo '$arquivo' não existe."
        return 1
    fi

    sed -n "${numero}p" "$arquivo"
}

coluna() {
    if [ $# -ne 2 ]; then
        echo "Uso: coluna <numero> <arquivo>"
        return 1
    fi

    local numero=$1
    local arquivo=$2

    if [ ! -f "$arquivo" ]; then
        echo "O arquivo '$arquivo' não existe."
        return 1
    fi

    cut -d: -f$numero "$arquivo"
}

