#!/bin/sh

# Verifica se foram passados exatamente 3 argumentos
[ "$#" -ne 3 ] && echo "Por favor, insira exatamente 3 números como argumentos." && exit 1

# Realiza a soma utilizando o comando 'bc'
s=$(echo "$1 + $2 + $3" | bc)

# Imprime o resultado
echo "$s"
