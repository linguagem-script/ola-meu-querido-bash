#!/bin/sh

# Verifica se foram passados exatamente 4 argumentos
[ "$#" -ne 4 ] && echo "Por favor, insira exatamente 4 nomes como argumentos." && exit 1

# Cria os diretórios e os arquivos README.md
mkdir "$1" "$2" "$3" "$4"
date +"# $1%nData: %Y-%m-%d" > "$1/README.md"
date +"# $2%nData: %Y-%m-%d" > "$2/README.md"
date +"# $3%nData: %Y-%m-%d" > "$3/README.md"
date +"# $4%nData: %Y-%m-%d" > "$4/README.md"
