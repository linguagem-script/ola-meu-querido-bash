#!/bin/sh

# Função para imprimir uma poesia
print_frase() {
    echo "$1"
    sleep 1
}

# Poesia
print_frase "No jardim da vida, florescendo amor,"
print_frase "Entre sonhos e risos, a alma a dançar."
print_frase "Na melodia do tempo, a esperança é calor,"
print_frase "E a luz da jornada nos guia a sonhar."

print_frase "Estrelas cintilam na noite serena,"
print_frase "Sussurros de paz, a lua a cantar."
print_frase "Nos passos incertos, a fé nos sustena,"
print_frase "E a vida se tece num doce bordar."

print_frase "Caminhamos unidos, na estrada da vida,"
print_frase "Com coragem e fé, cada passo a trilhar."
