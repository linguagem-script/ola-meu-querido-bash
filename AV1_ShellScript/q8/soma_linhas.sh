#!/bin/sh

# Verifica se foram passados exatamente 3 argumentos
[ "$#" -ne 3 ] && echo "Por favor, insira exatamente 3 nomes de arquivos como argumentos." && exit 1

# Usa a substituição de shell para contar as linhas de cada arquivo
l1=$(wc -l < "$1")
l2=$(wc -l < "$2")
l3=$(wc -l < "$3")

# Soma os números de linhas
s=$((l1 + l2 + l3))

# Imprime a soma
echo "$s"
