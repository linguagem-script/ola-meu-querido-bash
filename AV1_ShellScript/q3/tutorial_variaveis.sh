#!/bin/sh

# Explicação de como criar variáveis no Bash

# 1. Atribuição direta
a=10
b="Olá Mundo"
c=true

# 2. Atribuição através do comando 'read'
echo "Por favor, insira um valor para a variável 'd':"
read d

# 3. Atribuição por parâmetros de linha de comando
# Por exemplo, se o script for chamado como: ./script.sh 20
e=$1

# Explicação da diferença entre entrada do usuário e parâmetros de linha de comando
echo "Variável 'd' (solicitada ao usuário): $d"
echo "Variável 'e' (recebida como argumento): $e"

# Explicação de variáveis automáticas
echo "Nome do script: $0"
echo "Número total de argumentos: $#"
echo "Todos os argumentos passados: $@"
echo "Primeiro argumento: $1"
echo "Segundo argumento: $2"

