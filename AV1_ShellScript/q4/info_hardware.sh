#!/bin/sh

# Informações sobre o processador
echo "----- Informações sobre o Processador -----"
lscpu | grep "Model name:" | cut -d':' -f2 | tr -s ' '
lscpu | grep "CPU(s):" | cut -d':' -f2 | tr -s ' '
echo "------------------------------------------"

# Informações sobre a memória
echo "----- Informações sobre a Memória -----"
cat /proc/meminfo | grep "MemTotal:" | tr -s ' '
echo "--------------------------------------"

# Informações sobre os discos
echo "----- Informações sobre os Discos -----"
df -h | grep -E "/dev/sd|Filesystem" | tr -s ' '
echo "--------------------------------------"

# Informações sobre a placa-mãe
echo "----- Informações sobre a Placa-Mãe -----"
lshw -class baseboard | grep "product" | cut -d':' -f2 | tr -s ' '
lshw -class baseboard | grep "serial" | cut -d':' -f2 | tr -s ' '
echo "----------------------------------------"

# Informações sobre a BIOS
echo "----- Informações sobre a BIOS -----"
lshw -class bios | grep "vendor" | cut -d':' -f2 | tr -s ' '
lshw -class bios | grep "version" | cut -d':' -f2 | tr -s ' '
lshw -class bios | grep "date" | cut -d':' -f2 | tr -s ' '
echo "-----------------------------------"
echo "Script utilizável somente como superusuário"
echo "OBS:Em alguns casos o script não fornece as informações da placa-mãe e da BIOS pois essas configurações podem não está disponíveis para a leitura"
