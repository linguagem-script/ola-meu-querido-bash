#!/bin/sh

# Solicita os nomes dos diretórios
echo "Por favor, insira o nome do primeiro diretório:"
read d1
echo "Por favor, insira o nome do segundo diretório:"
read d2
echo "Por favor, insira o nome do terceiro diretório:"
read d3

# Encontra e conta os arquivos com extensões .xls, .bmp e .docx
x=$(find "$d1" "$d2" "$d3" -type f -name "*.xls" | wc -l)
b=$(find "$d1" "$d2" "$d3" -type f -name "*.bmp" | wc -l)
d=$(find "$d1" "$d2" "$d3" -type f -name "*.docx" | wc -l)

# Imprime os resultados
echo "Quantidade de arquivos .xls: $x"
echo "Quantidade de arquivos .bmp: $b"
echo "Quantidade de arquivos .docx: $d"
