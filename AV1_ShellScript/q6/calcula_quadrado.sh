#!/bin/sh

# Solicita ao usuário para digitar um número inteiro
echo "Por favor, digite um número inteiro:"
read x

# Calcula o quadrado de x e adiciona 7 para obter y
y=$((x * x + 7))

# Imprime os valores de x e y
echo "O valor de x é: $x"
echo "O valor de y é: $y"
