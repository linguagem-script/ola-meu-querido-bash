#!/bin/bash

# Verifica se o usuário é "ifpb"
if [ "$USER" != "ifpb" ]; then
  echo "Este script deve ser executado como usuário ifpb."
  exit 1
fi

# Verifica se foi passado um argumento de linha de comando
if [ $# -eq 0 ]; then
  caminho="/home/ifpb"
else
  caminho="$1"
fi

# Verifica se o caminho especificado é válido
if [ ! -e "$caminho" ]; then
  echo "O caminho especificado não existe."
  exit 1
fi

# Conta a quantidade de arquivos e diretórios
qtd_arquivos=$(find "$caminho" -type f | wc -l)
qtd_diretorios=$(find "$caminho" -type d | wc -l)

# Exibe o resultado
echo "Quantidade de arquivos em $caminho: $qtd_arquivos"
echo "Quantidade de diretórios em $caminho: $qtd_diretorios"

