#!/bin/bash

# Obtém o caminho absoluto do diretório atual
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# Caminho para func.sh
FUNC_PATH="$SCRIPT_DIR/func.sh"

# Inclui o script func.sh
source "$FUNC_PATH"

# Restaura o prompt para a versão inicial
configure_prompt "0" "\u"
echo "Prompt restaurado para a versão inicial."

