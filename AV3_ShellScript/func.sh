#!/bin/bash

# Função para configurar o prompt com cor e nome do usuário
configure_prompt() {
    local color="$1"
    local username="$2"
    PS1="\[\e[1;${color}m\]${username}\[\e[0m\] \n\$ "

    # Adiciona as configurações no ~/.bashrc
    echo -e "\n# Configurações adicionadas automaticamente pelo script func.sh" >> ~/.bashrc
    echo 'PS1="\[\e[1;'"$color"'m\]'"$username"'\[\e[0m\] \n\$ "' >> ~/.bashrc
}

# Configura o prompt inicial no ~/.bashrc
configure_prompt "0" "\u"

