#!/bin/bash

# Obtém o caminho absoluto do diretório atual
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# Caminho para func.sh
FUNC_PATH="$SCRIPT_DIR/func.sh"

# Inicializações padrão
COLOR_DEFAULT=0  # Cor padrão (preto)
USERNAME_DEFAULT="\u"  # Nome do usuário padrão

# Função para exibir as opções de personalização
show_customization_options() {
    echo "Opções de personalização:"
    echo "1. Cor do Prompt"
    echo "2. Nome do Usuário"
    echo "3. Ambos"
    echo "4. Nenhum (Restaurar para o padrão)"
    echo "0. Sair"
}

# Função para obter a escolha do usuário
get_customization_choice() {
    read -p "Escolha uma opção (0-4): " choice
    echo "$choice"
}

# Chama a função principal
source "$FUNC_PATH"
configure_initial_prompt() {
    configure_prompt "$COLOR_DEFAULT" "$USERNAME_DEFAULT"
    echo "Prompt configurado para a versão inicial."
}

configure_initial_prompt

