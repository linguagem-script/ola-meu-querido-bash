#!/bin/bash

# Obtém o caminho absoluto do diretório atual
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# Caminho para func.sh
FUNC_PATH="$SCRIPT_DIR/func.sh"

# Inclui o script func.sh
source "$FUNC_PATH"

# Função para exibir as opções de personalização
show_customization_options() {
    echo "Opções de personalização:"
    echo "1. Cor do Prompt"
    echo "2. Nome do Usuário"
    echo "3. Ambos"
    echo "4. Nenhum (Restaurar para o padrão)"
    echo "0. Sair"
}

# Função para obter a escolha do usuário
get_customization_choice() {
    read -p "Escolha uma opção (0-4): " choice
    echo "$choice"
}

# Exibe as opções de personalização
show_customization_options

# Obtém a escolha do usuário
choice=$(get_customization_choice)

# Aplica a personalização com base na escolha do usuário
case $choice in
    1)
        read -p "Digite o código da cor (0-255): " color_code
        configure_prompt "$color_code" "$USERNAME_DEFAULT"
        ;;
    2)
        read -p "Digite o novo nome do usuário: " new_username
        configure_prompt "$COLOR_DEFAULT" "$new_username"
        ;;
    3)
        read -p "Digite o código da cor (0-255): " color_code
        read -p "Digite o novo nome do usuário: " new_username
        configure_prompt "$color_code" "$new_username"
        ;;
    4)
        configure_prompt "$COLOR_DEFAULT" "$USERNAME_DEFAULT"
        ;;
    0)
        echo "Saindo sem fazer alterações."
        ;;
    *)
        echo "Opção inválida. Saindo."
        ;;
esac

