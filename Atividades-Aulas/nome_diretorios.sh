#!/bin/bash

# Verificando se foram fornecidos exatamente dois argumentos
if [ "$#" -ne 2 ]; then
    echo "Por favor, forneça exatamente dois diretórios como argumentos."
    exit 1
fi

# Atribuindo os argumentos a e b
dir1=$1
dir2=$2

# Verificando se os diretórios existem
if [ ! -d "$dir1" ] || [ ! -d "$dir2" ]; then
    echo "Um ou ambos os diretórios não existem."
    exit 1
fi

# Listando o conteúdo dos diretórios e salvando em o3.txt
{
    ls "$dir1"
    echo "-----"
    ls "$dir2"
} > o3.txt

echo "Conteúdo dos diretórios $dir1 e $dir2 salvos em o3.txt."

cat o3.txt
