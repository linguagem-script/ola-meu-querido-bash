#!/bin/bash

# Obter informações do clima
function obter_clima() {
    local cidade=$1
    curl -s "wttr.in/${cidade}?format=%C+%t+%w"
}

# Obter a data e hora atual
data=$(date +"%d/%m/%Y")
hora=$(date +"%H:%M:%S")

# Obter informações do clima para João Pessoa e Recife
clima_joao_pessoa=$(obter_clima "João+Pessoa")
clima_recife=$(obter_clima "Recife")

# Exibir informações
echo "Data: ${data}"
echo "Hora: ${hora}"
echo "Clima em João Pessoa: ${clima_joao_pessoa}"
echo "Clima em Recife: ${clima_recife}"

