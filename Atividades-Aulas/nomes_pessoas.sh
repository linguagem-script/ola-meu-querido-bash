#!/bin/env bash

# Ler dois nomes como parâmetro de linha de comando

read -p "Digite o primeiro nome: " nome1
read -p "Digite o segundo nome: " nome2

# Verificar se o nome não é vazio
[ -z "$nome1" ] && exit 0
[ -z "$nome2" ] && exit 0

# Escrever os nomes em ordem alfabética
[ "$nome1" < "$nome2" ] && echo $nome1 $nome2 || echo $nome2 $nome1
