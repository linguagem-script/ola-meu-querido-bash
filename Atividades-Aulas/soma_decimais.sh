#!/usr/bin/env python3
import sys

# Verificando se foram fornecidos exatamente dois argumentos
if len(sys.argv) != 3:
    print("Por favor, forneça exatamente dois números decimais como argumentos.")
    sys.exit(1)

# Obtendo os números decimais dos argumentos de linha de comando
try:
    a = float(sys.argv[1])
    b = float(sys.argv[2])
except ValueError:
    print("Os argumentos fornecidos não são números decimais válidos.")
    sys.exit(1)

# Calculando a soma
soma = a + b

# Imprimindo o resultado
print(soma)
