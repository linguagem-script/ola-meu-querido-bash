#!/bin/env bash

test -e $1 && echo $1 Existe!
test -e $1 || echo $1 Não Existe!
test -e $1 || exit 1

test -e $2 && echo $2 Existe!
test -e $2 || echo $2 Não Existe!
test -e $2 || exit 1

x=$(wc -l < $1)
y=$(wc -l < $2)

test $x -gt $y && echo $1 || echo $2
