#!/bin/bash

# 1. $HOME
echo "1. \$HOME: Contém o caminho do diretório home do usuário atual."

# 2. $PWD
echo -e "\n2. \$PWD: Contém o caminho completo do diretório atual (presente de trabalho)."

# 3. $USER
echo -e "\n3. \$USER: Contém o nome do usuário atual."

# 4. $UID
echo -e "\n4. \$UID: Contém o ID do usuário atual."

# 5. $SHELL
echo -e "\n5. \$SHELL: Contém o caminho para o shell atual."

# 6. $HOSTNAME
echo -e "\n6. \$HOSTNAME: Contém o nome do host (máquina) atual."

# 7. $OSTYPE
echo -e "\n7. \$OSTYPE: Contém o tipo do sistema operacional."

# 8. $RANDOM
echo -e "\n8. \$RANDOM: Gera um número aleatório entre 0 e 32767."

# 9. $PPID
echo -e "\n9. \$PPID: Contém o ID do processo pai."

# 10. $LINENO
echo -e "\n10. \$LINENO: Contém o número da linha atual no script."

# 11. $? 
echo -e "\n11. \$?: Contém o status de saída do último comando."

# 12. $0
echo -e "\n12. \$0: Contém o nome do próprio script."

# 13. $#
echo -e "\n13. \$#: Contém o número de argumentos passados para o script ou função."

# 14. $1, $2, ..., $n (Argumentos)
echo -e "\n14. \$1, \$2, ..., \$n: Contêm os argumentos passados para o script ou função."

# 15. $@
echo -e "\n15. \$@: Contém todos os argumentos passados para o script ou função como uma lista separada."

# 16. $$
echo -e "\n16. \$\$: Contém o ID do processo atual (PID)."

# 17. $IFS
echo -e "\n17. \$IFS: Contém o Internal Field Separator, usado para separar palavras em variáveis."

# 18. $OLDPWD
echo -e "\n18. \$OLDPWD: Contém o caminho do diretório anterior."

# 19. $SECONDS
echo -e "\n19. \$SECONDS: Contém o número de segundos desde que o script foi iniciado."

# 20. $FUNCNAME
echo -e "\n20. \$FUNCNAME: Contém o nome da função atual (dentro de uma função)."

