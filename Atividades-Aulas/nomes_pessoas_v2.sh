#!/bin/env bash
# Ler dois nomes 
read -p "Digite o 1° nome: " nome1
read -p "Digite o 2° nome: " nome2

# Verificar se está vazio
if [ -z "$nome1"] || [ -z "$nome2"]; then
	echo "Ta vazio"
else
	# Colocar em ordem alfabética
	if [ $nome1 < $nome2  ]; then
	       	echo $nome1 $nome2 	
	else
		echo $nome2 $nome1
	fi
fi

