#!/bin/bash

# Verifica se foram passados exatamente dois argumentos
if [ "$#" -ne 2 ]; then
    echo "Erro: Por favor, forneça exatamente dois números inteiros como argumentos."
    exit 1
fi

# Atribui os dois argumentos a e b
a=$1
b=$2

# Solicita ao usuário que digite uma operação
echo "Por favor, digite a operação (+,-,*,/,**):"
read op

# Verifica qual operação foi inserida e realiza a operação correspondente
case $op in
    +)
        resultado=$(($a + $b))
        ;;
    -)
        resultado=$(($a - $b))
        ;;
    \*)
        resultado=$(($a * $b))
        ;;
    /)
        resultado=$(($a / $b))
        ;;
    **)
        resultado=$(($a ** $b))
        ;;
    *)
        echo "Operação inválida. Por favor, use +, -, *, / ou **."
        exit 1
        ;;
esac

# Imprime o resultado
echo "O resultado de $a $op $b é igual a $resultado."
