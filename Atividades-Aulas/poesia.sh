#!/bin/bash

# Função para imprimir uma linha colorida com pausa
function imprimir_linha_colorida() {
    local cor=$1
    local texto=$2

    echo -e "\e[${cor}m${texto}\e[0m"
    sleep 1.0
}

# Poesia de Carlos Drummond de Andrade
versos=(
    "No meio do caminho tinha uma pedra"
    "Tinha uma pedra no meio do caminho"
    "Tinha uma pedra"
    "No meio do caminho tinha uma pedra"
)

cores=("31" "32" "33" "34") # Cores para os versos

# Itera pelos versos e os imprime
for i in "${!versos[@]}"; do
    imprimir_linha_colorida "${cores[$i]}" "${versos[$i]}"
done
