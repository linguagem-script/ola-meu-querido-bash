#!/bin/env bash

# Loop para criar 3 arquivos
for i in {1..3}
do
	# Gerar número aleatório
	num=$((RANDOM % 12 + 2))

	# Verifica se o arquivo existe
	if [ -e "${num}.txt" ]; then
		# Se existir imprimir mensagem em vermelho e encerra o script
		echo -e "\e[31mErro: O arquivo ${numero}.txt já existe.\e[0m"
		exit 1
	else
		# Caso contrário cria o arquivo
		touch "${num}.txt"
	fi
done

echo "Arquivo gerado com sucesso!"
