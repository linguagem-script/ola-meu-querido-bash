#!/bin/env bash

# Loop para criar 3 arquivos
for i in {1..3}
do
	# Gera um número aleatório entre 2 e 13
	numero=$((RANDOM % 12 + 2))

	# Cria o arquivo com um nome aleatório
	touch "${numero}.txt"
done

echo "Arquivos gerados!"
