#!/bin/env bash 

# Inicializa contadores
existentes=0
nao_existentes=0

# Loop para percorrer os parâmetros de linha de comando
# $@ verifica todos os parâmetros passados
for arquivo in "$@"; do
    if [ -e "$arquivo" ]; then
        existentes=$((existentes + 1))
    else
        nao_existentes=$((nao_existentes + 1))
    fi
done

# Imprime a quantidade de arquivos existentes e não existentes
echo "Quantidade de arquivos existentes: $existentes"
echo "Quantidade de arquivos não existentes: $nao_existentes"
